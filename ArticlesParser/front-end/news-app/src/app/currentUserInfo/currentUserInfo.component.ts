import { Component, OnInit } from '@angular/core';
// import { UserService } from '../_services/user.service';
// import { User } from '../_models/user';
import { CurrentUser } from '../_models/currentUser';
import { AuthenticationService } from '../_services/authentication.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-current-user-info',
  templateUrl: './currentUserInfo.component.html',
  styleUrls: ['./currentUserInfo.component.css']
})
export class CurrentUserInfoComponent implements OnInit {
  currentUser: CurrentUser;
  constructor(
    // private userService: UserService,
    private authenticationService: AuthenticationService // ,
    // private router: Router
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
}

logout(): void {
  this.authenticationService.logout();
  this.currentUser = null;
  location.reload(true);
}

ngOnInit(): void {}
}
