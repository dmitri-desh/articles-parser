import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service';
import { AppConfig } from '../app.config';
// import { NewsModel } from '../_models/news';
// import { AuthenticationService } from '../_services/authentication.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './allNews.component.html',
  styleUrls: ['./allNews.component.css']
})
export class AllNewsComponent implements OnInit {
  news: any[];
  userActive: boolean;
  constructor(
    private newsService: ApiService,
    private urlNews: AppConfig // ,
    // private authenticate: AuthenticationService,
    // private router: Router
    ) {}

  ngOnInit(): void {
   // if(this.authenticate.canActivate()){
     // this.userActive=true;
    this.newsService.getData(this.urlNews.apiAllNewsEndpoint)
                    .subscribe(data => this.news = data as any[]);
   // } else {
   //  this.router.navigate(['/not_access']);
    // this.userActive = false;
   // }
  }

}
