import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service';
import { AppConfig } from '../app.config';
// import { AuthenticationService } from '../_services/authentication.service';
// import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-good-news',
  templateUrl: './goodNews.component.html',
  styleUrls: ['./goodNews.component.css']
})
export class GoodNewsComponent implements OnInit {

  news: any[];
  userActive: boolean;
  constructor(
    private newsService: ApiService,
    private urlNews: AppConfig // ,
    // private authenticate: AuthenticationService,
    // private router: Router
    ) {}

  ngOnInit(): void {
    this.newsService.getData(this.urlNews.apiGoodNewsEndpoint)
                    .subscribe(data => this.news = data as any[]);
  }

}
