export interface NewsModel {
  Id: number;
  SiteId: number;
  PostTitle: string;
  PostText: string;
  PostUrl: string;
  PostHtml: string;
}
