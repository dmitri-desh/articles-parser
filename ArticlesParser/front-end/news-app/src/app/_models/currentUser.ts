export class CurrentUser {
    UserName: string;
    Role: string;
    Token: string;
    Message: string;
}
