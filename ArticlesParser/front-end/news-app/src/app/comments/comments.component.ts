import { Component, OnInit, Input } from '@angular/core';
import { AppConfig } from '../app.config';
import { ApiService } from '../_services/api.service';
import { CurrentUser } from '../_models/currentUser';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
@Input() idPost: string;
currentUser: CurrentUser;
comments: any[];
state = true;
commentButtonText = 'Смотреть комментарии';
  constructor(
    private newsService: ApiService,
    private urlComments: AppConfig,
    private http: HttpClient) { }

  ngOnInit(): void {}

  getCommentsByIdPost(idPost: string): void {
    this.newsService.getData(this.urlComments.apiCommentsEndpoint + idPost)
                    .subscribe(data => this.comments = data as any[]);
    this.state = !this.state;
    this.commentButtonText = this.state ? 'Смотреть комментарии' : 'Скрыть комментарии';
  }

  deleteComment(id: string, idPost: string): void {
    if (window.confirm('Вы, действительно, хотите удалить комментарий?')) {
    this.http.delete(this.urlComments.apiDeleteCommentEndpoint + id)
    .subscribe(
      response => {
        alert('Комментарий удалён успешно');
        this.newsService.getData(this.urlComments.apiCommentsEndpoint + idPost)
                        .subscribe(data => this.comments = data as any[]);
      },
      (err: HttpErrorResponse) => {
        alert(err.error['Message']);
      });
    }
  }

  isVisibleButton() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser && this.currentUser.Role === 'Admin') {
      return false;
    }
    return true;
  }
}
