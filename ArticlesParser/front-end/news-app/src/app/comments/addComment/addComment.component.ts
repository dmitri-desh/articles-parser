import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../_services/api.service';
import { AppConfig } from '../../app.config';
import { CurrentUser } from '../../_models/currentUser';
import { FormGroup, FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AlertService } from '../../_services/alert.service';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-add-comment',
  templateUrl: './addComment.component.html',
  styleUrls: ['./addComment.component.css']
})
export class AddCommentComponent implements OnInit {
  @Input() idPost: string;
  commentForm: FormGroup;
  currentUser: CurrentUser;
  commentText: string;
  state = true;
  visible = true;
  commentButtonText = 'Добавить комментарий';

  constructor(
    private urlComments: AppConfig,
    private formBuilder: FormBuilder,
    private hhtp: HttpClient,
    private alertService: AlertService,
    private auth: AuthenticationService
    ) { }

  ngOnInit(): void {
    this.visible = !this.auth.canActivate();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.commentForm = this.formBuilder.group({
      commentText: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(200)]],
      newsPostId: this.idPost,
      author: this.currentUser != null ? this.currentUser.UserName : null
    });
  }

  notTypedText() {
    if (!this.commentText) { return true; }
    return false;
  }

  get f() { return this.commentForm.controls; }

  addComment(): void {
    this.hhtp.post(this.urlComments.apiAddCommentEndpoint, this.commentForm.value)
      .subscribe(
        response => {
          this.state = true;
          this.commentButtonText = 'Комментарий успешно добавлен';
        },
        (err: HttpErrorResponse) => {
          this.alertService.error(err.error['Message'], true);
        });
  }

  showHideCommentForm() {
    this.state = !this.state;
    this.commentButtonText = this.state ? 'Добавить комментарий' : 'Скрыть';
  }
}
