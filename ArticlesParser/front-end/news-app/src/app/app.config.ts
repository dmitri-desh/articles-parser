import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
  })
export class AppConfig {
    apiAllNewsEndpoint = 'http://localhost:52800/api/news/GetNews' ;
    apiGoodNewsEndpoint = 'http://localhost:52800/api/news/GetGoodNews' ;
    apiNeutralNewsEndpoint = 'http://localhost:52800/api/news/GetNeutralNews' ;
    apiBadNewsEndpoint = 'http://localhost:52800/api/news/GetBadNews' ;
    apiCommentsEndpoint = 'http://localhost:52800/api/comments/GetComment/';
    apiRegisterUserEndpoint = 'http://localhost:52800/api/Auth/RegisterUser';
    apiLoginUserEndpoint = 'http://localhost:52800/api/Auth/AuthUser';
    apiAddCommentEndpoint = 'http://localhost:52800/api/Comments/PostComment';
    apiDeleteCommentEndpoint = 'http://localhost:52800/api/Comments/DeleteComment/';
}
