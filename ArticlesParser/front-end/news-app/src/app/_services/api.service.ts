import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

constructor(private http: HttpClient) { }

public getData(url: string): Observable<any> {
  const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
  return this.http.get(url, httpOptions);
}
}
