import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../_models/user';
import { AppConfig } from '../app.config';
// import { Response } from '@angular/http';

@Injectable()
export class UserService {
    constructor(private http: HttpClient, private apiUrl: AppConfig) { }

    getById(id: number) { return this.http.get(`/users/` + id); }

    register(user: User) {
        return this.http.post(this.apiUrl.apiRegisterUserEndpoint, user);
    }
}
