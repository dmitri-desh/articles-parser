import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig } from '../app.config';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient, private apiUrl: AppConfig) { }

    login(username: string, password: string) {
        return this.http.post<any>(this.apiUrl.apiLoginUserEndpoint, { username: username, password: password })
            .pipe(map(user => {
                if (user && user['Token']) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            return user;
        }));
    }

    canActivate() {
        if (localStorage.getItem('currentUser')) {
            return true;
        }
        return false;
    }

    logout(): void {
        localStorage.removeItem('currentUser');
    }
}
