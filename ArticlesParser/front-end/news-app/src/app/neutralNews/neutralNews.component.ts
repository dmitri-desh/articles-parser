import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service';
import { AppConfig } from '../app.config';
// import { AuthenticationService } from '../_services/authentication.service';
// import { Router } from '@angular/router';

@Component({
  selector: 'app-neutral-news',
  templateUrl: './neutralNews.component.html',
  styleUrls: ['./neutralNews.component.css']
})
export class NeutralNewsComponent implements OnInit {
  news: any[];
  userActive: boolean;
  constructor(
    private newsService: ApiService,
    private urlNews: AppConfig // ,
    // private authenticate: AuthenticationService,
    // private router: Router
  ) {}

  ngOnInit(): void {
    this.newsService.getData(this.urlNews.apiNeutralNewsEndpoint)
                    .subscribe(data => this.news = data as any[]);
  }
}
