import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { ApiService } from './_services/api.service';
import { AppConfig } from './app.config';
import { CommentsComponent } from './comments/comments.component';
import {RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import {APP_BASE_HREF} from '@angular/common';
import { AlertComponent } from './alert/alert.component';
import { UserService } from './_services/user.service';
import { AlertService } from './_services/alert.service';
import { AuthenticationService } from './_services/authentication.service';
import { CurrentUserInfoComponent } from './currentUserInfo/currentUserInfo.component';
import { LayoutComponent } from './layout/layout.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { InviteForAuthenticationComponent } from './inviteForAuthentication/inviteForAuthentication.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { AllNewsComponent } from './allNews/allNews.component';
import { GoodNewsComponent } from './goodNews/goodNews.component';
import { NeutralNewsComponent } from './neutralNews/neutralNews.component';
import { BadNewsComponent } from './badNews/badNews.component';
import { AddCommentComponent } from './comments/addComment/addComment.component';
import { CurrentUser } from './_models/currentUser';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'registration',
        component: RegistrationComponent
    },
    {
        path: 'not_access',
        component: InviteForAuthenticationComponent
    },
    {
        path: 'all_news',
        component: AllNewsComponent
    },
    {
        path: 'good_news',
        component: GoodNewsComponent
    },
    {
        path: 'neutral_news',
        component: NeutralNewsComponent
    },
    {
        path: 'bad_news',
        component: BadNewsComponent
    },
    {
        path: '**', redirectTo: '/'
    }
];

@NgModule({
   declarations: [
      AppComponent,
      AllNewsComponent,
      CommentsComponent,
      LoginComponent,
      RegistrationComponent,
      AlertComponent,
      CurrentUserInfoComponent,
      LayoutComponent,
      NavbarComponent,
      InviteForAuthenticationComponent,
      GoodNewsComponent,
      NeutralNewsComponent,
      BadNewsComponent,
      AddCommentComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      NgxPaginationModule,
      RouterModule.forRoot(routes),
      BsDropdownModule.forRoot(),
      FormsModule
   ],
   providers: [
       ApiService,
       AppConfig,
       CurrentUser,
       CurrentUserInfoComponent,
       {
           provide: APP_BASE_HREF,
           useValue : '/'
        },
       UserService,
       AlertService,
       AuthenticationService,
       {
           provide: HTTP_INTERCEPTORS,
           useClass: JwtInterceptor,
           multi: true
        },
       {
           provide: HTTP_INTERCEPTORS,
           useClass: ErrorInterceptor,
           multi: true
        }
],
   bootstrap: [
    AppComponent
   ]
})
export class AppModule { }
