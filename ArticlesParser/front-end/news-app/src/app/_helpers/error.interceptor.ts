import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
// import { CurrentUserInfoComponent } from '../currentUserInfo/currentUserInfo.component';
import { AlertService } from '../_services/alert.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private alertservice: AlertService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            const error = err.error['Message'] || err.statusText;
            if (err.status === 401) {
                this.router.navigate(['/not_access']);
                this.authService.logout();
            } else {
                this.alertservice.error(error);
            }
            return throwError(error);
        }));
    }
}
