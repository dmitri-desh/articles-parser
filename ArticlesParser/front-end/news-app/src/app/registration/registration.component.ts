import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({templateUrl: 'registration.component.html',
})
export class RegistrationComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService
      ) { }

    ngOnInit(): void {
        this.registerForm = this.formBuilder.group({
            username: ['', [Validators.required, Validators.maxLength(20)]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
            email: ['', [Validators.required, Validators.email]]
        });
    }

     get f() { return this.registerForm.controls; }

    onSubmit(): void {
        this.submitted = true;

        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.register(this.registerForm.value)
            .subscribe(
                response => {
                    alert(response['Message']);
                    this.alertService.success(response['Message'], true);
                    this.router.navigate(['/login']);
                },
                (err: HttpErrorResponse) => {
                   // this.alertService.error(err.error['Message'], true);
                    this.loading = false;
                });
    }
}
