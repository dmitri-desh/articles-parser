﻿using System;
using System.Collections.Generic;
using System.Configuration;
using JWT.Algorithms;
using JWT;
using JWT.Serializers;
using DAL.Models;

namespace BLL
{
    public class TokenManager
    {
        string secret = ConfigurationManager.AppSettings["Secret"];

        public Token EncodeToken(int userId, string userName, string role)
        {

            var issuedOn = DateTime.Now;
            var expiriesOn = DateTimeOffset.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpiry"])).ToUnixTimeSeconds();
            DateTime toDbDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            toDbDateTime = toDbDateTime.AddSeconds(expiriesOn).ToLocalTime();
            var payload = new Dictionary<string, object>
            {
                { "exp", expiriesOn },
                { "UserName", userName },
                { "Role", role}
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var tokenKey = encoder.Encode(payload, secret);
            Token token = new Token
            {
                TokenKey = tokenKey,
                IssuedOn = issuedOn,
                ExpiresOn = toDbDateTime,
                UserId = userId
            };
            return token;
        }
        public bool IsValidToken(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    IJsonSerializer serializer = new JsonNetSerializer();
                    IDateTimeProvider provider = new UtcDateTimeProvider();
                    IJwtValidator validator = new JwtValidator(serializer, provider);
                    IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                    IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                    var json = decoder.Decode(token, secret, verify: true);
                    if (json == null)
                        return false;
                    else
                        return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public string DecodeToken(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    IJsonSerializer serializer = new JsonNetSerializer();
                    IDateTimeProvider provider = new UtcDateTimeProvider();
                    IJwtValidator validator = new JwtValidator(serializer, provider);
                    IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                    IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                    var json = decoder.Decode(token, secret, verify: true);
                    return json;
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}