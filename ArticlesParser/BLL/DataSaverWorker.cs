﻿using DAL.DTO;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL
{
    public class DataSaverWorker
    {
        private IUnitOfWork _unitOfWork;

        public DataSaverWorker(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddContentToDb(Tuple<ICollection<NewsDto>, ICollection<CommentDto>> content)
        {
            if (content.Item1 != null && content.Item1.Count > 0)
                foreach (var news in content.Item1)
                {
                    SaveData(news, content.Item2.Where(x => x.PostId == news.IdFromUrl).Select(x => x).ToList());
                }
        }

        private void SaveData(NewsDto newsDto, ICollection<CommentDto> commentsDto)
        {
            var site = new Site()
            {
                SiteName = newsDto.SiteName,
                SiteUrl = newsDto.SiteUrl
            };

            var siteId = _unitOfWork.Sites.GetId(site);
            if (siteId == null)
            {
                _unitOfWork.Sites.Create(site);
                _unitOfWork.Save();
                siteId = _unitOfWork.Sites.GetId(site);
            }
            var post = new NewsPost()
            {
                IdFromUrl = newsDto.IdFromUrl,
                PostHtml = newsDto.PostHtml,
                PostText = newsDto.PostText,
                PostTitle = newsDto.PostTitle,
                PostUrl = newsDto.PostUrl,
                SiteId = (int)siteId,
                CreateDate = DateTime.Now
            };
            var postIdFromUrl = _unitOfWork.NewsPosts.GetIdFromUrl(post);
            int postId = 0;
            if (_unitOfWork.NewsPosts.GetId(post) != null)
            { postId = (int)_unitOfWork.NewsPosts.GetId(post); }

            if (postIdFromUrl == null)
            {
                _unitOfWork.NewsPosts.Create(post);
                _unitOfWork.Save();
                postId = (int)_unitOfWork.NewsPosts.GetId(post);
            }
            if (commentsDto != null && commentsDto.Count > 0)
            {
                foreach (var comment in commentsDto)
                {
                    var commentExist = _unitOfWork.Comments.GetId(new Comment { Author = comment.Author, CommentText = comment.CommentText });
                    if (commentExist == null)
                    {
                        _unitOfWork.Comments.Create(new Comment
                        {
                            Author = comment.Author,
                            CommentText = comment.CommentText,
                            NewsPostId = postId
                        });
                        _unitOfWork.Save();
                    }
                }
            }
        }
        public async void SaveSentIndexes()
        {
            SentimentalIndex sentimentalIndex = new SentimentalIndex();
            var posts = _unitOfWork.NewsPosts
                                   .GetAll()
                                   .Where(x => x.SentIndex == 0 
                                            && x.CreateDate >= DateTime.Now.Date
                                         )
                                   .ToList();
            int i = 0;
            while (true)
            {
                var result = posts.Skip(i).Take(30).ToArray();
                if (result.Length > 0)
                {
                    foreach (var post in result)
                    {
                        var sentIndex = await sentimentalIndex.GetSentimentalIndexAsync(post.PostText);
                        _unitOfWork.NewsPosts.AddSentIndex(post.Id, sentIndex);
                        _unitOfWork.Save();
                    }
                    i = i + 30;
                    await Task.Delay(60000);
                }
                else
                {
                    break;
                }
            }
        }
        public bool SaveTokenToDb(Token token)
        {
            var result = _unitOfWork.Auth.InsertToken(token);
            if (result == 1)
                return true;
            else
                return false;
        }
    }
}
