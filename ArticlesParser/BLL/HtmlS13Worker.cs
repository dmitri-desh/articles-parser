﻿using System.IO;
using System.Net;
using System.Text;

namespace BLL
{
    public static class HtmlS13Worker
    {
        public static string GetS13PageHtml(string pageUrl)
        {
            string page;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pageUrl);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        page = reader.ReadToEnd();
                    }
                }
                return page;
            }
            catch
            {
                return null;
            }

        }
    }
}
