﻿using DAL.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace BLL
{
    class TextDeSerializeModel
    {
        public string text { get; set; }
        public AnnotationsTextDeSerializeModel annotations { get; set; }
    }
    class AnnotationsTextDeSerializeModel
    {
        public InfoTextDeSerializeModel[] lemma { get; set; }
    }
    class InfoTextDeSerializeModel
    {
        public string start { get; set; }
        public string end { get; set; }
        public string value { get; set; }
    }
    public class TextToWordsConverter : ITextToWordsConverter
    {
        public IList<string> GetWords(string text)
        {
            var apiIspras = @ConfigurationManager.AppSettings["apiIspras"];
            var wordsList = new List<string>();
            var preparedText = text.Replace("\"", "");
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiIspras);
            request.Content = new StringContent("[{\"text\":\"" + preparedText + "\"}]", Encoding.UTF8, "application/json");

            var req = client.SendAsync(request);
            if (req == null || req.Result == null) return wordsList;

            var result = req.Result.Content.ReadAsStringAsync().Result;
            if (result != "")
            {
                var jsonResult = result.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' });
                TextDeSerializeModel des = JsonConvert.DeserializeObject<TextDeSerializeModel>(jsonResult);
                foreach (var item in des.annotations.lemma)
                   wordsList.Add(item.value);
               
                wordsList = wordsList.Where(w => w != string.Empty && Regex.Match(w, @"^[^A-Za-z0-9]+").Success && w.Length > 1).Select(w => w).ToList();
            }
            return wordsList;
        }
    }
}