﻿using DAL.DTO;
using DAL.Interfaces;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class S13Parser : IParser
    {
        ICollection<NewsDto> _newsDto;
        ICollection<CommentDto> _commentDto;
        ILoggerHelper _loggerHelper;
        string postId;

        public S13Parser(ILoggerHelper loggerHelper)
        {
            _newsDto = new List<NewsDto>();
            _commentDto = new List<CommentDto>();
            postId = string.Empty;
            _loggerHelper = loggerHelper;
        }

        public Tuple<ICollection<NewsDto>, ICollection<CommentDto>> Parse(string url)
        {
            Tuple<ICollection<NewsDto>, ICollection<CommentDto>> result = null;
            try
            {
                _loggerHelper.Message($"Start parse - {url}");
                List<string> postsUrls = GetPostsUrls(url);
                foreach (var item in postsUrls)
                {
                    string page = HtmlS13Worker.GetS13PageHtml(item);
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(page);
                    postId = item.Substring(item.LastIndexOf('/') + 1, item.Length - item.LastIndexOf('/') - 1);
                    _newsDto.Add(new NewsDto
                    {
                        PostUrl = item,
                        PostTitle = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='itemhead']/h3/a").InnerText,
                        SiteName = new Uri(url).Host,
                        SiteUrl = url,
                        PostText = GetPostTextOrHtml(htmlDocument.DocumentNode.SelectSingleNode("//div[@class='item entry']").InnerHtml, true),
                        PostHtml = GetPostTextOrHtml(htmlDocument.DocumentNode.SelectSingleNode("//div[@class='item entry']").InnerHtml, false),
                        IdFromUrl = postId
                    });
                    if (htmlDocument.DocumentNode.SelectNodes("//ol[@class='commentlist']/li") != null)
                    {
                        foreach (var comment in htmlDocument.DocumentNode.SelectNodes("//ol[@class='commentlist']/li").ToList())
                        {
                            HtmlDocument commentNode = new HtmlDocument();
                            commentNode.LoadHtml(comment.InnerHtml);
                            string commentHtml = string.Empty;
                            StringBuilder builder = new StringBuilder();
                            foreach (var paragraf in commentNode.DocumentNode.SelectNodes("//span/*"))
                                if (paragraf.Name == "p" || paragraf.Name == "blockquote")
                                   builder.Append(paragraf.InnerHtml).Append(" ");

                            commentHtml = builder.ToString();
                            _commentDto.Add(new CommentDto
                            {
                                PostId = postId,
                                Author = commentNode.DocumentNode.SelectSingleNode("//span[@class='commentauthor']")
                                                                 .InnerText
                                                                 .Replace("&nbsp;", ""),
                                CommentText = commentHtml
                            });
                        }
                    }
                }
                _loggerHelper.Message($"End parse - {url}");
                result = Tuple.Create(_newsDto, _commentDto);

            }
            catch (Exception e)
            {
                _loggerHelper.Error($"Error parse - {url} {e.Message}");
            }
            return result;
        }

        private string GetPostTextOrHtml(string html, bool IsText)
        {
            string postText;
            StringBuilder builder = new StringBuilder();
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            try
            {
                foreach (var paragraf in htmlDocument.DocumentNode.SelectNodes("//div[@class='itemtext js-mediator-article']/*"))
                    if (paragraf.Name == "p" || paragraf.Name == "blockquote")
                        if (IsText)
                            builder.Append(paragraf.InnerText).Append(" ");
                        else
                            builder.Append(paragraf.InnerHtml).Append(" ");

            }
            catch
            {
                throw;
            }
            return postText = builder.ToString();
        }

        private List<string> GetPostsUrls(string url)
        {
            List<string> postsUrls = new List<string>();
            try
            {
                string page = HtmlS13Worker.GetS13PageHtml(url);
                if (page == null)
                {
                    _loggerHelper.Error($"Filed load url - {url}");
                    return null;
                }
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(page);
                foreach (var node in htmlDocument.DocumentNode.SelectNodes("//div[@class='itemhead']").ToList())
                {
                    HtmlDocument htmlnode = new HtmlDocument();
                    htmlnode.LoadHtml(node.InnerHtml);
                    postsUrls.Add(htmlnode.DocumentNode.SelectSingleNode("//h3/a").GetAttributeValue("href", ""));
                }
            }
            catch
            {
                throw;
            }
            return postsUrls;
        }
    }
}
