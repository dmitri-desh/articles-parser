﻿using DAL.DTO;
using DAL.Interfaces;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml;

namespace BLL
{
    public class TutByRSSParser : IParser
    {
        ICollection<NewsDto> _newsDto;
        ICollection<CommentDto> _commentDto;
        ILoggerHelper _loggerHelper;
        string postId;

        public TutByRSSParser(ILoggerHelper loggerHelper)
        {
            _newsDto = new List<NewsDto>();
            _commentDto = new List<CommentDto>();
            postId = string.Empty;
            _loggerHelper = loggerHelper;
        }

        public Tuple<ICollection<NewsDto>, ICollection<CommentDto>> Parse(string url)
        {
            Tuple<ICollection<NewsDto>, ICollection<CommentDto>> result = null;
            _loggerHelper.Message($"Start parse - {url}");
            try
            {
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                foreach (var item in feed.Items)
                {
                    postId = new Uri(item.Id).AbsolutePath;
                    _newsDto.Add(new NewsDto
                    {
                        PostUrl = item.Id,
                        PostTitle = item.Title.Text,
                        SiteName = new Uri(url).Host,
                        SiteUrl = url,
                        PostText = GetPostTextOrHtml(item.Summary.Text, true),
                        PostHtml = GetPostTextOrHtml(item.Summary.Text, false),
                        IdFromUrl = GetArticleIdBy(item.Id) //postId
                    });
                }
                _loggerHelper.Message($"End parse - {url}");
                result = Tuple.Create(_newsDto, _commentDto);
            }

            catch (Exception e)
            {
                _loggerHelper.Error($"Error parse - {url} {e.Message}");
            }
            return result;
        }
        private string GetPostTextOrHtml(string html, bool IsText)
        {
            string postText;
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            try
            {
                if (IsText)
                   postText = htmlDocument.DocumentNode.InnerText;
                else
                   postText = html;
            }
            catch
            {
                throw;
            }
            return postText;
        }

        public static string GetArticleIdBy(string url)
        {
            string fileName = string.Empty;
            Uri uri = new Uri(url);
            fileName = System.IO.Path.GetFileNameWithoutExtension(uri.LocalPath);
            string resultString = string.Empty;
            try
            {
                Regex regexObj = new Regex(@"[^\d+$]");
                resultString = regexObj.Replace(fileName, "");
            }
            catch (ArgumentException ex)
            {
                throw;
            }
            return resultString;
        }
    }
}
