﻿using DAL.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;

namespace BLL
{
    public class SentimentalIndex
    {
        private readonly ITextToWordsConverter _textToWordsConverter = new TextToWordsConverter();
        IDictionary<string, int> _wordsDictionary;

        public SentimentalIndex()
        {
            LoadJsonAsync();
        }

        private async void LoadJsonAsync()
        {
            string rootPath = @AppDomain.CurrentDomain.BaseDirectory;
            string wordsFileJsonPath = @Path.Combine(rootPath, "App_Data", "words.json");
            using (StreamReader r = new StreamReader(wordsFileJsonPath))
            {
                string json = await r.ReadToEndAsync();
                _wordsDictionary = JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
            }
        }

        public Task<int> GetSentimentalIndexAsync(string text)
        {
            double result = 0;
            int index = 0;
            int value = 0;
            int i = 0;
            return Task.Run(() =>
           {
               IList<string> words = _textToWordsConverter.GetWords(text);

               foreach (var word in words)
                   if (_wordsDictionary.TryGetValue(word, out value))
                   {
                       index += value;
                       i++;
                   }
               
               if (i > 0)
                   result = Math.Round((double)index / (double)i, 2);
               if (result > GetEpsilon())
                   index = 2; // good news
               if (result < -GetEpsilon())
                   index = -1; // bad news
               if (Math.Abs(result) <= GetEpsilon())
                   index = 1; // neutral news
               return index;
           });
        }

        static double GetEpsilon()
        {
            var doubleStr = ConfigurationManager.AppSettings["epsilon"];
            double value;
            bool check = Double.TryParse(doubleStr, out value);
            return value;
        }
    }
}
