﻿using BLL;
using DAL.Interfaces;
using Quartz;
using Autofac;
using System.Threading.Tasks;
using System.Configuration;

namespace WebApiApp.Jobs
{
    public class TutByRssParseJob : IJob
    {
       // const string urlS13 = "http://s13.ru";
      //  const string urlRssTutBy = "https://news.tut.by/rss/index.rss";
        private readonly ILoggerHelper _loggerHelper = Util.AutofacConfig.ConfigureContainer().Resolve<ILoggerHelper>();
        private readonly IUnitOfWork _unitOfWork = Util.AutofacConfig.ConfigureContainer().Resolve<IUnitOfWork>();

        public async Task Execute(IJobExecutionContext context)
        {
            var tutByRssUrl = @ConfigurationManager.AppSettings["tutByRssUrl"];
          //  S13Parser s13Parser = new S13Parser(_loggerHelper);
            DataSaverWorker dataWorker = new DataSaverWorker(_unitOfWork);
//            dataWorker.AddContentToDb(s13Parser.Parse(s13Url));

            TutByRSSParser tutByRSSParser = new TutByRSSParser(_loggerHelper);
            dataWorker.AddContentToDb(tutByRSSParser.Parse(tutByRssUrl));
        }
    }
}
