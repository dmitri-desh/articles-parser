﻿using Autofac;
using BLL;
using DAL.Interfaces;
using Quartz;
using System.Threading.Tasks;

namespace WebApiApp.Jobs
{
    public class SentimentalIndexCalcJob : IJob
    {

        private readonly ILoggerHelper _loggerHelper = Util.AutofacConfig.ConfigureContainer().Resolve<ILoggerHelper>();
        private readonly IUnitOfWork _unitOfWork = Util.AutofacConfig.ConfigureContainer().Resolve<IUnitOfWork>();

        public async Task Execute(IJobExecutionContext context)
        {
            DataSaverWorker dataWorker = new DataSaverWorker(_unitOfWork);
            dataWorker.SaveSentIndexes();
        }
    }
}