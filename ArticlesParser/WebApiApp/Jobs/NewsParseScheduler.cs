﻿using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;

namespace WebApiApp.Jobs
{
    public class NewsParseScheduler
    {
        public static async void Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            int numValue;
            bool parsed = Int32.TryParse(ConfigurationManager.AppSettings["period"], out numValue);

            IJobDetail job1 = JobBuilder.CreateForAsync<S13ParseJob>()
                                        .WithIdentity("job1")
                                        .Build();
            ITrigger trigger1 = TriggerBuilder.Create()
                                             .WithIdentity("trigger1")
                                             .StartNow()
                                             .WithSimpleSchedule(x => x.WithIntervalInMinutes(numValue)
                                                                       .RepeatForever())
                                             .Build();

            IJobDetail job2 = JobBuilder.CreateForAsync<TutByRssParseJob>()
                                        .WithIdentity("job2")
                                        .Build();
            ITrigger trigger2 = TriggerBuilder.Create()
                                            .WithIdentity("trigger2")
                                            .StartNow()
                                            .WithSimpleSchedule(x => x.WithIntervalInMinutes(numValue)
                                                                      .RepeatForever())
                                            .Build();

            IJobDetail job3 = JobBuilder.CreateForAsync<SentimentalIndexCalcJob>()
                                       .WithIdentity("job3")
                                       .Build();
            ITrigger trigger3 = TriggerBuilder.Create()
                                            .WithIdentity("trigger3")
                                            .StartNow()
                                            .WithSimpleSchedule(x => x.WithIntervalInMinutes(numValue)
                                                                      .RepeatForever())
                                            .Build();

            await scheduler.ScheduleJob(job1, trigger1);
            await scheduler.ScheduleJob(job2, trigger2);
            await scheduler.ScheduleJob(job3, trigger3);
        }
    }
}
