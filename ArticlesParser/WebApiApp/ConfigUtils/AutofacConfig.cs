﻿using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using System.Web.Http;
using LoggerLib;

namespace WebApiApp.ConfigUtils
{
    public static class AutofacConfig
    {
        public static IContainer ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<DAL.UnitOfWork>().As<Core.Interfaces.IUnitOfWork>();
            builder.RegisterType<LoggerHelper>().As<Core.Interfaces.ILoggerHelper>();
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            return container;
        }
    }
}