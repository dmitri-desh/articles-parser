﻿using AutoMapper;
using Core.Models;
using WebApiApp.Models;

namespace WebApiApp.ConfigUtils
{
    public static class AutoMapperConfig
    {
        public static void GetMapper()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<NewsPost, PostModel>();
                cfg.CreateMap<Comment, CommentModel>();
                cfg.CreateMap<Site, SiteModel>();
                cfg.CreateMap<User, RegisterUserModel>();
                cfg.CreateMap<User, AuthUserModel>();
            });
        }
    }
}