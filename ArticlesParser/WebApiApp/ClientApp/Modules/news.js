﻿var newsApp = angular.module('news', []);

newsApp.controller('newsCtrl', function ($scope, $http) {
    $http.get('http://localhost:52800/api/news').then(function (response) {
        if (response.status == 200) {
            $scope.posts = response.data;
        }
    });
});
