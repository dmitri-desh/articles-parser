﻿namespace WebApiApp.Models
{
    public class RegistrationResponseModel
    {
        public string User { get; set; }
        public string Message { get; set; }
    }
}