﻿using System.ComponentModel.DataAnnotations;

namespace WebApiApp.Models
{
    public class AuthUserModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
    }
   
}