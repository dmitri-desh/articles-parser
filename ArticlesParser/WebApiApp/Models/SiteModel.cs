﻿namespace WebApiApp.Models
{
    public class SiteModel
    {
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string SiteUrl { get; set; }
    }
}