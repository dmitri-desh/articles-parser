﻿namespace WebApiApp.Models
{
    public class PostModel
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string PostTitle { get; set; }
        public string PostUrl { get; set; }
        public string PostText { get; set; }
        public string PostHtml { get; set; }
        public double SentIndex { get; set; }
    }
}