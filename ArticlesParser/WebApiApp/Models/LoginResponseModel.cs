﻿namespace WebApiApp.Models
{
    public class LoginResponseModel
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }
    }
}