﻿using System.ComponentModel.DataAnnotations;

namespace WebApiApp.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string CommentText { get; set; }
        public int NewsPostId { get; set; }
    }
}