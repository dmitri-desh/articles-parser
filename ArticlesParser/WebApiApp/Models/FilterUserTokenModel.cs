﻿namespace WebApiApp.Models
{
    public class FilterUserTokenModel
    {
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}