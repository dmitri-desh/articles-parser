﻿using WebApiApp.Util;
using System.Web.Http;

namespace WebApiApp
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfig.GetMapper();

            Jobs.NewsParseScheduler.Start();
        }
    }
}
