﻿using Autofac;
using BLL;
using DAL.Interfaces;
using WebApiApp.Util;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WebApiApp.Filters
{
    public class APIAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly IUnitOfWork _unitOfWork;
        IContainer Container = AutofacConfig.ConfigureContainer();

        public APIAuthorizeAttribute()
        {
            _unitOfWork = Container.Resolve<IUnitOfWork>();
        }
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (IsAuthorized(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext);
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            try
            {
                TokenManager tm = new TokenManager();
                var token = actionContext.Request.Headers.GetValues("Token").First();
                return tm.IsValidToken(token);
            }
            catch
            {
                return false;
            }
        }
    }
}