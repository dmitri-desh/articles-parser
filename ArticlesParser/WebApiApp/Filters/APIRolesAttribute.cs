﻿using Autofac;
using BLL;
using DAL.Interfaces;
using WebApiApp.Models;
using WebApiApp.Util;
using Newtonsoft.Json;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WebApiApp.Filters
{
    public class APIRolesAttribute : AuthorizeAttribute
    {
        private readonly IUnitOfWork _unitOfWork;
        IContainer Container = AutofacConfig.ConfigureContainer();
        private readonly string[] allowedRoles;   //todo make a multiple roles

        public APIRolesAttribute(params string[] roles)
        {
            allowedRoles = roles;
            _unitOfWork = Container.Resolve<IUnitOfWork>();
        }
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (IsAuthorized(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext);
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            TokenManager tm = new TokenManager();
            bool authorize = false;
            var token = actionContext.Request.Headers.GetValues("Token").First();
            if (!string.IsNullOrEmpty(token))
            {
                var json = tm.DecodeToken(token);
                if (string.IsNullOrEmpty(json)) return authorize;
                FilterUserTokenModel userName = JsonConvert.DeserializeObject<FilterUserTokenModel>(json);
                foreach (var role in allowedRoles)
                {
                    var user = _unitOfWork.Auth.GetUserByRole(role, userName.UserName);
                    if (user != null)
                    {
                        authorize = true;
                    }
                }

            }
            return authorize;
        }
    }
}