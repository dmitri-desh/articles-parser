﻿using AutoMapper;
using BLL;
using DAL.Interfaces;
using DAL.Models;
using WebApiApp.Models;
using Newtonsoft.Json;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiApp.Controllers.Account
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Auth")]
    public class AuthController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public AuthController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [Route("AuthUser")]
        public HttpResponseMessage Post([FromBody]AuthUserModel user)
        {
            string responseObject;
            if (ModelState.IsValid)
            {
                AuthUserModel authUser = Mapper.Map<User, AuthUserModel>(_unitOfWork.Auth.GetUser(user.UserName, user.Password));
                if (authUser == null)
                {

                    if (!_unitOfWork.Users.IsUserExist(user.UserName))
                    {
                        responseObject = JsonConvert.SerializeObject(new LoginResponseModel { Message = "User " + user.UserName + " not found" });
                        var message = new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                        return message;
                    }
                    else
                    {
                        responseObject = JsonConvert.SerializeObject(new LoginResponseModel { Message = "Password not valid" });
                        var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                        return message;
                    }
                }
                else
                {
                    var userToken = _unitOfWork.Auth.GetUser(user.UserName, user.Password);
                    if (_unitOfWork.Auth.IsTokenExists(userToken.UserName))
                    {
                        _unitOfWork.Auth.DeleteToken(userToken.UserName);
                        return AddToken(userToken.UserId, userToken.UserName, userToken.Role.RoleName);
                    }
                    else
                    {
                        return AddToken(userToken.UserId, userToken.UserName, userToken.Role.RoleName);
                    }
                }
            }
            else
            {
                responseObject = JsonConvert.SerializeObject(new LoginResponseModel { Message = "Not Valid Request" });
                var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                return message;
            }
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
        private HttpResponseMessage AddToken(int userId, string userName, string role)
        {
            TokenManager tm = new TokenManager();
            DataSaverWorker saverWorker = new DataSaverWorker(_unitOfWork);
            Token token = tm.EncodeToken(userId, userName, role);

            if (token.TokenKey != null)
            {
                var result = saverWorker.SaveTokenToDb(token);
                HttpResponseMessage response = new HttpResponseMessage();
                var responseObject = JsonConvert.SerializeObject(new LoginResponseModel { UserName = userName, Role = role, Token = token.TokenKey });
                response = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                response.Headers.Add("Token", token.TokenKey);
                response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["TokenExpiry"]);
                response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                return response;
            }
            else
            {
                var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable);
                message.Content = new StringContent("Error in Creating Token");
                return message;
            }
        }
    }
}