﻿using DAL.Interfaces;
using SecurityLib;
using DAL.Models;
using WebApiApp.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiApp.Controllers.Account
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/Auth")]
    public class RegisterUserController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public RegisterUserController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET api/<controller>

        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        [Route("RegisterUser")]
        public HttpResponseMessage Post([FromBody]RegisterUserModel user)
        {
            string responseObject;
            if (ModelState.IsValid)
            {
                if (_unitOfWork.Users.IsUserExist(user.UserName))
                {
                    responseObject = JsonConvert.SerializeObject(new RegistrationResponseModel { User = user.UserName, Message = "User " + user.UserName + " is already registered" });
                    var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                    return message;
                }
                _unitOfWork.Users.Add(new User
                {
                    CreatedOn = DateTime.Now,
                    Email = user.Email,
                    Password = Encryption.Encrypt(user.Password),
                    RoleId = 2,
                    UserName = user.UserName
                });
                _unitOfWork.Save();
                responseObject = JsonConvert.SerializeObject(new RegistrationResponseModel { User = user.UserName, Message = "User " + user.UserName + " registered successfully" });
                var messageOk = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                return messageOk;
            }
            else
            {
                responseObject = JsonConvert.SerializeObject(new RegistrationResponseModel { User = "", Message = "Not Valid Request" });
                var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                return message;
            }
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}