﻿using AutoMapper;
using DAL.Interfaces;
using DAL.Models;
using WebApiApp.Filters;
using WebApiApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiApp.Controllers
{
    /// <summary>
    /// News
    /// </summary>

    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class NewsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public NewsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Get all news
        /// </summary>
        /// <returns></returns>
        // GET api/News/GetNews

        public IEnumerable<PostModel> GetNews()
        {
            IEnumerable<PostModel> postModels = Mapper.Map<IEnumerable<NewsPost>, IEnumerable<PostModel>>(_unitOfWork.NewsPosts.GetAll().OrderByDescending(p => p.Id));
            return postModels;
        }

        /// <summary>
        /// Get all news
        /// </summary>
        /// <returns></returns>
        // GET api/News/GetGoodNews
        [APIAuthorize]
        public IEnumerable<PostModel> GetGoodNews()
        {
            IEnumerable<PostModel> postModels = Mapper.Map<IEnumerable<NewsPost>, IEnumerable<PostModel>>(_unitOfWork.NewsPosts.GetGoodNews().OrderByDescending(p => p.Id));
            return postModels;
        }
        [APIAuthorize]
        public IEnumerable<PostModel> GetNeutralNews()
        {
            IEnumerable<PostModel> postModels = Mapper.Map<IEnumerable<NewsPost>, IEnumerable<PostModel>>(_unitOfWork.NewsPosts.GetNeutralNews().OrderByDescending(p => p.Id));
            return postModels;
        }

        public IEnumerable<PostModel> GetBadNews()
        {
            IEnumerable<PostModel> postModels = Mapper.Map<IEnumerable<NewsPost>, IEnumerable<PostModel>>(_unitOfWork.NewsPosts.GetBadNews().OrderByDescending(p => p.Id));
            return postModels;
        }

        /// <summary>
        /// Get news by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/News/5
        public IHttpActionResult GetNews(int id)
        {
            PostModel postModel = Mapper.Map<NewsPost, PostModel>(_unitOfWork.NewsPosts.GetById(id));
            if (postModel == null)
            {
                return NotFound();
            }
            return Ok(postModel);
        }



        // POST api/News
        public void PostNews([FromBody]string value)
        {
        }

        // PUT api/News/5
        public void PutNews(int id, [FromBody]string value)
        {
        }

        // DELETE api/News/5
        public void DeleteNews(int id)
        {
        }
    }
}
