﻿using AutoMapper;
using DAL.Interfaces;
using DAL.Models;
using WebApiApp.Filters;
using WebApiApp.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebApiApp.Controllers
{
    /// <summary>
    /// Sites
    /// </summary>
    [APIAuthorize]
    public class SitesController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        
        public SitesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Get all sites
        /// </summary>
        /// <remarks>
        /// Get all sites
        /// </remarks>
        /// <returns></returns>
        // GET: api/Sites
        public IEnumerable<SiteModel> GetSites()
        {
            IEnumerable<SiteModel> SiteModels = Mapper.Map<IEnumerable<Site>, IEnumerable<SiteModel>>(_unitOfWork.Sites.GetAll());
            return SiteModels;
        }
        /// <summary>
        /// Get site by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Sites/5
        [ResponseType(typeof(SiteModel))]
        public IHttpActionResult GetSite(int id)
        {
            SiteModel SiteModel = Mapper.Map<Site, SiteModel>(_unitOfWork.Sites.GetById(id));
            if (SiteModel == null)
            {
                return NotFound();
            }
            return Ok(SiteModel);
        }

        // PUT: api/Sites/5
        [ResponseType(typeof(void))]
        public void PutSite(int id, SiteModel SiteModel)
        {
        }

        // POST: api/Sites
        [ResponseType(typeof(SiteModel))]
        public void PostSite(SiteModel SiteModel)
        {
        }

        // DELETE: api/Sites/5
        [ResponseType(typeof(SiteModel))]
        public void DeleteSite(int id)
        {
        }
    }
}
