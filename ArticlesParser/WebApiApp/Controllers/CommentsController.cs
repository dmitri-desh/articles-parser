﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AutoMapper;
using DAL.Interfaces;
using DAL.Models;
using WebApiApp.Filters;
using WebApiApp.Models;
using Newtonsoft.Json;

namespace WebApiApp.Controllers
{
    /// <summary>
    /// Comments
    /// </summary>
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CommentsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommentsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Get all comments
        /// </summary>
        /// <returns></returns>
        // GET: api/Comments
        public IEnumerable<CommentModel> GetComments()
        {
            IEnumerable<CommentModel> commentsModels = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentModel>>(_unitOfWork.Comments.GetAll());
            return commentsModels;
        }
        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Comments/5
        [ResponseType(typeof(CommentModel))]
        public IHttpActionResult GetComment(int id)
        {
            IEnumerable<CommentModel> commentsModels = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentModel>>(_unitOfWork.Comments.GetAnyById(id));
            if (commentsModels == null)
            {
                return NotFound();
            }

            return Ok(commentsModels);
        }

        // PUT: api/Comments/5
        [ResponseType(typeof(void))]
        public void PutComment(int id, CommentModel commentModel)
        {
        }

        // POST: api/Comments
        [APIAuthorize]
        [ResponseType(typeof(CommentModel))]
        public HttpResponseMessage PostComment([FromBody]CommentModel CommentModel)
        {
            string responseObject;
            if (ModelState.IsValid)
            {
                _unitOfWork.Comments.Create(new Comment
                {
                    NewsPostId = CommentModel.NewsPostId,
                    Author = CommentModel.Author,
                    CommentText = CommentModel.CommentText
                });
                _unitOfWork.Save();
                responseObject = JsonConvert.SerializeObject(new { Message = "Comment from User " + CommentModel.Author + "added successfully" });
                var messageOk = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
                return messageOk;
            }
            responseObject = JsonConvert.SerializeObject(new { Message = "Comment not added" });
            var message = new HttpResponseMessage(HttpStatusCode.NotAcceptable) { Content = new StringContent(responseObject, System.Text.Encoding.UTF8, "application/json") };
            return message;
        }

        // DELETE: api/Comments/5
        [APIAuthorize]
        [APIRoles("Admin")]
        [ResponseType(typeof(CommentModel))]
        public IHttpActionResult DeleteComment(int id)
        {
            _unitOfWork.Comments.Delete(id);
            _unitOfWork.Save();
            return Ok();
        }
    }
}