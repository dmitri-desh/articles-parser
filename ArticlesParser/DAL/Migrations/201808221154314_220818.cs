namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class _220818 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Author = c.String(),
                        CommentText = c.String(),
                        NewsPostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewsPosts", t => t.NewsPostId, cascadeDelete: true)
                .Index(t => t.NewsPostId);
            
            CreateTable(
                "dbo.NewsPosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdFromUrl = c.String(),
                        PostUrl = c.String(),
                        PostTitle = c.String(),
                        PostText = c.String(),
                        PostHtml = c.String(),
                        SentIndex = c.Double(nullable: false),
                        SiteId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sites", t => t.SiteId, cascadeDelete: true)
                .Index(t => t.SiteId);
            
            CreateTable(
                "dbo.Sites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SiteName = c.String(),
                        SiteUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        TokenID = c.Int(nullable: false, identity: true),
                        TokenKey = c.String(),
                        IssuedOn = c.DateTime(nullable: false),
                        ExpiresOn = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TokenID)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tokens", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.NewsPosts", "SiteId", "dbo.Sites");
            DropForeignKey("dbo.Comments", "NewsPostId", "dbo.NewsPosts");
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Tokens", new[] { "UserId" });
            DropIndex("dbo.NewsPosts", new[] { "SiteId" });
            DropIndex("dbo.Comments", new[] { "NewsPostId" });
            DropTable("dbo.Users");
            DropTable("dbo.Tokens");
            DropTable("dbo.Roles");
            DropTable("dbo.Sites");
            DropTable("dbo.NewsPosts");
            DropTable("dbo.Comments");
        }
    }
}
