﻿using DAL.Models;
using System.Data.Entity;

namespace DAL
{
    public class NewsContext: DbContext
    {
        public NewsContext() : base("DbNewsConnection")
        {
            Database.SetInitializer(new DBInitializer());
        }
        public DbSet<Site> Sites { get; set; }
        public DbSet<NewsPost> NewsPosts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Token> Tokens { get; set; }

    }
}
