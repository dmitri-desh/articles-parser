﻿using System;

namespace DAL.Models
{
    public class Token
    {
        public int TokenID { get; set; }
        public string TokenKey { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime ExpiresOn { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
