﻿namespace DAL.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string CommentText { get; set; }
        public int NewsPostId { get; set; }
        public virtual NewsPost NewsPost { get; set; }
    }
}
