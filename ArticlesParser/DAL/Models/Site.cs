﻿namespace DAL.Models
{
    public class Site
    {
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string SiteUrl { get; set; }
    }
}
