﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class NewsPost
    {
        public int Id { get; set; }
        public string IdFromUrl { get; set; }
        public string PostUrl { get; set; }
        public string PostTitle { get; set; }
        public string PostText { get; set; }
        public string PostHtml { get; set; }
        public double SentIndex { get; set; }
        public int SiteId { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }
        public virtual Site Site { get; set; }
        public virtual IList<Comment> Comments { get; set; }
    }
}
