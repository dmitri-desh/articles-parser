﻿using DAL.Interfaces;
using DAL.Models;
using SecurityLib;
using System;
using System.Linq;

namespace DAL.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly NewsContext _context;

        public AuthRepository(NewsContext context)
        {
            _context = context;
        }
        public User GetUser(string userName, string userPassword)
        {
            try
            {
                var pass = Encryption.Encrypt(userPassword);
                var result = _context.Users.Include("Role").FirstOrDefault(x => x.UserName == userName && x.Password == pass);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public User GetUserByRole(string roleName, string userName)
        {
            try
            {
                var result = _context.Users.Include("Role").FirstOrDefault(x => x.UserName == userName && x.Role.RoleName == roleName);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string GetRoleByUserName(string userName)
        {
            try
            {
                var roleName = _context.Users.Include("Role").FirstOrDefault(x => x.UserName == userName).Role.RoleName;
                return roleName;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Token GetToken(string userName)
        {
            try
            {
                Token token = _context.Tokens.FirstOrDefault(x => x.User.UserName == userName);
                return token;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool IsTokenExists(string userName)
        {
            try
            {
                var result = _context.Tokens.FirstOrDefault(x => x.User.UserName == userName);
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int InsertToken(Token token)
        {
            try
            {
                _context.Tokens.Add(token);
                return _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int DeleteToken(string userName)
        {
            try
            {
                var token = _context.Tokens.SingleOrDefault(x => x.User.UserName == userName);
                _context.Tokens.Remove(token);
                return _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
