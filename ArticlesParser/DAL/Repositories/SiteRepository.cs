﻿using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class SiteRepository : IRepository<Site>
    {
        private readonly NewsContext _context;
        public SiteRepository(NewsContext context)
        {
            _context = context;
        }
        public void Create(Site item)
        {
            _context.Sites.Add(item);
        }

        public IEnumerable<Site> GetAll()
        {
            return _context.Sites;
        }

        public Site GetById(int id)
        {
            return _context.Sites.FirstOrDefault(x => x.Id == id);
        }

        public int? GetId(Site item)
        {
            var tmp = _context.Sites.FirstOrDefault(x => x.SiteUrl == item.SiteUrl);
            if(tmp==null)
            {
                return null;
            }
            return tmp.Id;
        }
    }
}
