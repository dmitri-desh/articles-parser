﻿using DAL.Interfaces;
using DAL.Models;
using System.Linq;

namespace DAL.Repositories
{
    public class RegisterUserRepository : IRegisterUserRepository
    {
        private readonly NewsContext _context;

        public RegisterUserRepository(NewsContext context)
        {
            _context = context;
        }

        public void Add(User user)
        {
            _context.Users.Add(user);
        }

        public int GetUserId(string userName)
        {
            var result = _context.Users.FirstOrDefault(x => x.UserName == userName).UserId;
            return result;
        }

        public bool IsUserExist(string userName)
        {
            var result = _context.Users.FirstOrDefault(x => x.UserName == userName);
            if (result!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
