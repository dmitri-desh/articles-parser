﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly NewsContext _context;

        public CommentRepository(NewsContext context)
        {
            _context = context;
        }

        public void Create(Comment item)
        {
            _context.Comments.Add(item);
        }
        public void Delete(int id)
        {
           var comment= _context.Comments.FirstOrDefault(x=>x.Id == id);
            if(comment!=null)
            {
                _context.Comments.Remove(comment);
            }
        }

        public IEnumerable<Comment> GetAll()
        {
            return _context.Comments;
        }

        public IEnumerable<Comment> GetAnyById(int id)
        {
            return _context.Comments.Where(c => c.NewsPostId == id);
        }

        public Comment GetById(int id)
        {
            return _context.Comments.FirstOrDefault(x => x.Id == id);
        }

        public int? GetId(Comment item)
        {
            var tmp = _context.Comments.FirstOrDefault(x => x.Author == item.Author && x.CommentText == item.CommentText);
            if (tmp == null)
            {
                return null;
            }
            return tmp.Id;
        }

        Comment IRepository<Comment>.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
