﻿using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    public class NewsPostRepository : INewsPostRepository
    {
        private readonly NewsContext _context;
        
        public NewsPostRepository(NewsContext context)
        {
            _context = context;
        }

        public void AddSentIndex(int id, double index)
        {
            var tmp = GetById(id);
            if (tmp != null)
                tmp.SentIndex = index;
        }

        public void Create(NewsPost item)
        {
            _context.NewsPosts.Add(item);
        }

        public IEnumerable<NewsPost> GetAll() =>
                                                  _context.NewsPosts;
        
        public IEnumerable<NewsPost> GetGoodNews() =>
                                                  _context.NewsPosts
                                                          .Where(x => x.SentIndex == 2);
        
        public IEnumerable<NewsPost> GetNeutralNews() =>
                                                  _context.NewsPosts
                                                          .Where(x => x.SentIndex == 1);
        
        public IEnumerable<NewsPost> GetBadNews() =>
                                                  _context.NewsPosts
                                                          .Where(x => x.SentIndex == -1);

        public NewsPost GetById(int id) =>
                 _context.NewsPosts.FirstOrDefault(x => x.Id == id);
        
        public int? GetId(NewsPost item)
        {
            var tmp = _context.NewsPosts.FirstOrDefault(x => x.IdFromUrl == item.IdFromUrl);
            if (tmp == null)
               return null;
            return tmp.Id;
        }
        public string GetIdFromUrl(NewsPost item)
        {
            var tmp = _context.NewsPosts.FirstOrDefault(x => x.IdFromUrl == item.IdFromUrl);
            if (tmp == null)
               return null;
            return tmp.IdFromUrl;
        }

        public double GetSentIndex(int postId)
        {
            var tmp = _context.NewsPosts.FirstOrDefault(x => x.Id == postId);
            return Convert.ToDouble(tmp.SentIndex);
        }
    }
}
