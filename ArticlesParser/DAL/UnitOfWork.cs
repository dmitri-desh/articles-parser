﻿using DAL.Interfaces;
using DAL.Models;
using DAL.Repositories;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private NewsContext _context;
        private IRepository<Site> _siteRepository;
        private INewsPostRepository _newsPostRepository;
        private ICommentRepository _commentRepository;
        private IRegisterUserRepository _registerUserRepository;
        private IAuthRepository _authRepository;

        public UnitOfWork()
        {
            _context = new NewsContext();
        }

        public IRepository<Site> Sites => _siteRepository ?? (_siteRepository = new SiteRepository(_context));

        public INewsPostRepository NewsPosts => _newsPostRepository ?? (_newsPostRepository = new NewsPostRepository(_context));

        public ICommentRepository Comments => _commentRepository ?? (_commentRepository = new CommentRepository(_context));
        public IRegisterUserRepository Users => _registerUserRepository ?? (_registerUserRepository = new RegisterUserRepository(_context));
        public IAuthRepository Auth => _authRepository ?? (_authRepository = new AuthRepository(_context));

        public void Dispose()
        {
            if (_context == null) return;
            _context.Dispose();
            _context = null;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
