﻿namespace DAL.DTO

{
  public class CommentDto
    {
        public string PostId { get; set; }
        public string Author { get; set; }
        public string CommentText { get; set; }
    }
}
