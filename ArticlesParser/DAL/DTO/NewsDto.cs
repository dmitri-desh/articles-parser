﻿namespace DAL.DTO

{
   public class NewsDto
    {
        public string SiteName { get; set; }
        public string SiteUrl { get; set; }
        public string IdFromUrl { get; set; }
        public string PostUrl { get; set; }
        public string PostTitle { get; set; }
        public string PostText { get; set; }
        public string PostHtml { get; set; }
    }
}
