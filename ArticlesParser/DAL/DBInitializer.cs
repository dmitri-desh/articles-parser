﻿using SecurityLib;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DAL
{
    public class DBInitializer : DropCreateDatabaseIfModelChanges<NewsContext>
                                // CreateDatabaseIfNotExists<NewsContext>
    {
        protected override void Seed(NewsContext context)
        {
            IList<Role> defaultRoles = new List<Role>();

            defaultRoles.Add(new Role() { RoleId=1,RoleName="Admin"});
            defaultRoles.Add(new Role() { RoleId = 2, RoleName = "User" });
            context.Roles.AddRange(defaultRoles);
            User defaultUser = new User { UserName = "Dimon", Password = Encryption.Encrypt("dimpas01"), Email = "mail@mail.ru" , RoleId = 1, CreatedOn = DateTime.Now};
            context.Users.Add(defaultUser);
            base.Seed(context);
        }
    }
}
