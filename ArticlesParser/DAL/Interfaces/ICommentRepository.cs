﻿using System.Collections.Generic;
using DAL.Models;

namespace DAL.Interfaces
{
    public interface ICommentRepository:IRepository<Comment>
    {
        IEnumerable<Comment> GetAnyById(int id);
        void Delete(int id);
    }
}
