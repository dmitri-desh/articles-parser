﻿using DAL.Models;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface INewsPostRepository : IRepository<NewsPost>
    {
        string GetIdFromUrl(NewsPost item);
        void AddSentIndex(int id, double index);
        double GetSentIndex(int postId);
        IEnumerable<NewsPost> GetGoodNews();
        IEnumerable<NewsPost> GetNeutralNews();
        IEnumerable<NewsPost> GetBadNews();
    }
}
