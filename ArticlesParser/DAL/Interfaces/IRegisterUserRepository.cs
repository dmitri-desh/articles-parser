﻿using DAL.Models;

namespace DAL.Interfaces
{
    public interface IRegisterUserRepository
    {
        void Add(User user);
        bool IsUserExist(string userName);
        int GetUserId(string userName);
    }
}
