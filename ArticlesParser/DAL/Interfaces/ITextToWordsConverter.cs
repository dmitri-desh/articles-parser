﻿using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface ITextToWordsConverter
    {
        IList<string> GetWords(string text);
    }
}
