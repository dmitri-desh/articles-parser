﻿using DAL.Models;
using System;

namespace DAL.Interfaces
{
    public interface IUnitOfWork:IDisposable
    {
        IRepository<Site> Sites { get; }
        INewsPostRepository NewsPosts { get; }
        ICommentRepository Comments { get; }
        IRegisterUserRepository Users { get; }
        IAuthRepository Auth { get; }
        void Save();
    }
}
