﻿using DAL.Models;

namespace DAL.Interfaces
{
    public interface IAuthRepository
    {
        User GetUser(string userName, string userPassword);
        User GetUserByRole(string roleName, string userName);
        string GetRoleByUserName(string userName);
        Token GetToken(string userName);
        bool IsTokenExists(string userName);
        int InsertToken(Token token);
        int DeleteToken(string userName);
    }
}
