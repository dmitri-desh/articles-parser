﻿using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Create(T item);
        int? GetId(T item);
        IEnumerable<T> GetAll();
        T GetById(int id);
    }
}
