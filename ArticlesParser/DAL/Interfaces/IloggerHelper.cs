﻿namespace DAL.Interfaces
{
    public interface ILoggerHelper
    {
        void Message(string message);
        void Warning(string message);
        void Error(string message);
    }
}
