﻿using DAL.DTO;
using System;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface IParser
    {
        Tuple<ICollection<NewsDto>, ICollection<CommentDto>> Parse(string url);
    }
}
